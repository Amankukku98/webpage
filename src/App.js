import React from 'react'
import Navbar from './components/Navbar';
import SecondBox from './components/SecondBox';
import ThirdBox from './components/ThirdBox';
import FifthBox from './components/FifthBox';
function App() {
  return (
    <div>
      <Navbar/>
      <SecondBox/>
      <ThirdBox/>
      <FifthBox/>
    </div>
  )
}

export default App