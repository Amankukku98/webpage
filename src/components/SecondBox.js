import React from 'react'
import '../styles/SecondBox.css';
import Circle from './Circle';
function SecondBox() {
  return (
    <div className="second-box-container">
      <div className="second-first-inner-box">
        <div className="second-box-common second-box-image">
          <img src="https://www.shutterstock.com/image-photo/image-womans-hand-holding-bottle-260nw-2336462679.jpg" alt="" />
        </div>
        <div className="second-box-common second-box-contents">
          <p>Something title here that is not visible</p>
          <p id="title-header">We create strategic Webflow websites for passionate entrepreneurs</p>
          <button>Button Text</button>
        </div>
      </div>
      <div className="second-second-inner-box">
        <Circle/>
        <Circle/>
        <Circle/>
      </div>
    </div>
  )
}

export default SecondBox