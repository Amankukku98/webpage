import React from 'react'
import '../styles/ThirdBox.css';
function ThirdBox() {
  return (
    <div className="third-box">
      <div className="third-left-box third-common">
        <span>some text here</span>
        <p style={{fontSize:'24px'}}>But what exactly do we doyou may ask?</p>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptates optio nobis ipsa iusto voluptatibus mollitia, delectus at recusandae blanditiis? Magnam, vero? Voluptatum natus obcaecati provident earum, consectetur nemo ipsum quaerat.</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto inventore dolor fuga.</p>
        <button>Some Button Test</button>
      </div>
      <div className="third-right-box third-common">
        <img src="https://media.istockphoto.com/id/1181366400/photo/in-the-hands-of-trees-growing-seedlings-bokeh-green-background-female-hand-holding-tree-on.jpg?s=612x612&w=0&k=20&c=jWUMrHgjMY9zQXsAwZFb1jfM6KxZE16-IXI1bvehjQM=" alt="" />
      </div>
    </div>
  )
}

export default ThirdBox