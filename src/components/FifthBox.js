import React from 'react'
import '../styles/FifthBox.css';
import { MdEmail } from "react-icons/md";
import { LiaTwitter,LiaFacebook,LiaYoutube } from "react-icons/lia";
function FifthBox() {
  return (
    <div className="fifth-container">
      <div className="fifth-first-box">
        <h3>Our Latest Projects</h3>
      </div>
      <div className="fifth-second-box">
        <div className="card-container">
          <img src="nature.jpeg" alt="" />
          <p>Image Title</p>
        </div>
        <div className="card-container">
        <img src="nature1.jpeg" alt="" />
          <p>Image Title</p>
        </div>
        <div className="card-container">
        <img src="computer.jpeg" alt="" />
          <p>Image Title</p>
        </div>
      </div>
      <div className="fifth-third-box">
        <button>VIEW MORE COLLECTIONS</button>
      </div>
      <div className="fifth-fourth-box">
      <img src="pot1.jpeg" alt="" />
      <img src="pot2.jpeg" alt="" />
      <img src="computer1.jpeg" alt="" />
      <img src="pot3.jpeg" alt="" />
      <img src="pot4.jpeg" alt="" />
      </div>
      <div className="fifth-six-box">
       <span>
        <span>PRIMARY</span> <br />
        <span>____STORY</span>
       </span>
       <div className='fifth-six-inner-box'>
       <span>Option1</span>
       <span>Option2</span>
       <span>Option3</span>
       </div>
       <div style={{display:'flex',gap:'20px'}}>
        <LiaFacebook/>
        <MdEmail/>
        <LiaTwitter/>
        <LiaYoutube/>
       </div>
      </div>
      <div className="fifth-seven-box">
       <hr/>
       <div style={{display:'flex',justifyContent:'center',flexDirection:'column',alignItems:'center'}}>
        <p style={{display:'flex',gap:'20px'}}>
          <span>Option1</span><span>Option2</span>
        </p>
        <p style={{display:'flex',gap:'20px'}}>
          <span>Option1</span>
          <span>Option2</span>
          <span>Option3</span>
          <span>Option4</span>
        </p>
       </div>
      </div>
    </div>
  )
}

export default FifthBox