import React from 'react'
import '../styles/Navbar.css';
function Navbar() {
  return (
 <div className="nav-container">
    <div className="nav-title">
       <h6>PERFOMANCE <br/> __________STORY</h6>
    </div>
    <div className="nav-options">
       <ul>
        <li>Option-1</li>
        <li>Option-2</li>
        <li>Option-3</li>
       </ul>
    </div>
    <div className="nav-btn">
     <button>SAVE CART</button>
    </div>
 </div>
  )
}

export default Navbar